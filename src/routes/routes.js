import VueRouter from "vue-router";
import Login from "../pages/Login";
import Dashboard from "../pages/Dashboard";

const routes = [
    {
        path: '/',
        component: Login,
        redirect: '/login'
      },
    {
        path: '/login',
        component: Login,
    },
    {
        path: '/dashboard',
        component: Dashboard
    }
]
const router = new VueRouter({
    routes,
    mode: 'history'
})
export default router;