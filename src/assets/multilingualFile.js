const messages = {
    en: {
      message: {
        email: 'email',
        enter_email: 'Please Enter your email',
        password: 'password',
        submitLogin: 'submit',
        dashboard_header_name: '',
        dashboard_header_filed: '',
        dashboard_footer_organization: 'Iran Space Agency',
        dashboard_footer_edition: 'Edition 2.3.1',
        multiselect_placeholder_date: 'Pick a date',
        selected_layer_opacity: 'opacity',
        selected_layer_drag: 'drag',
        selected_layer_haeder: 'layers',
        date_alert_message: 'Date is required',
        delete_selected_layer_massage:'By clicking this icon the selected layer will remove from list',
        tools_header: 'Useful tools',
        tools_measure_item: 'measurement',
        tools_area_measurement: 'surface',
        tools_length_measurement: 'length',
        tools_area_measurement_draw: 'create',
        tools_area_measurement_draw_message: 'By clicking on map you can create a polygon',
        tools_area_measurement_edit: 'edit',
        tools_area_measurement_edit_message: 'Select the polygon to ba able to edit it',
        tools_area_measurement_delete: 'delete',
        tools_area_measurement_delete_message: 'click the selected polygon twice to be deleted from map',
        tools_length_measurement_draw: 'create',
        tools_length_measurement_draw_message: 'By clicking on map you can create a line',
        tools_length_measurement_edit: 'edit',
        tools_length_measurement_edit_message: 'Select the line to ba able to edit it',
        tools_length_measurement_delete: 'delete',
        tools_length_measurement_delete_message: 'click the selected line twice to be deleted from map'
      }
    },
    fa: {
      message: {
        email: 'ایمیل',
        enter_email: 'لطفا ایمیل خود را وارد نمایید',
        password: 'رمز عبور',
        submitLogin: 'ورود',
        dashboard_header_name: 'سامانه پایش ماهواره ای',
        dashboard_header_filed: 'محصولات کشاورزی',
        dashboard_footer_organization: 'سازمان فضایی ایران',
        dashboard_footer_edition: 'نگارش 2.3.1',
        multiselect_placeholder_date: 'از لیست زیر تاریخ را انتخاب کنید',
        selected_layer_opacity: 'میزان شفافیت',
        selected_layer_drag: 'کشیدن',
        selected_layer_haeder: 'لایه های انتخاب شده',
        date_alert_message: 'انتخاب تاریخ ضروری است',
        delete_selected_layer_massage: 'با کلیک این آیکن لایه انتخابی شما از لیست برداشته خواهد شد',
        tools_header: 'ابزار کاربردی',
        tools_measure_item: 'اندازه گیری',
        tools_area_measurement: 'سطحی',
        tools_length_measurement: 'طولی',
        tools_area_measurement_draw: 'ایجاد',
        tools_area_measurement_draw_message: 'برای رسم پلیگون بر روی نقشه کلیک کنید',
        tools_area_measurement_edit: 'ویرایش',
        tools_area_measurement_edit_message: 'برای ویرایش پلیگون بر روی آن کلیک کنید',
        tools_area_measurement_delete: 'حذف',
        tools_area_measurement_delete_message: 'برای حذف پلیگون دوبار روی آن کلیک کنید',
        tools_length_measurement_draw: 'ایجاد',
        tools_length_measurement_draw_message: 'برای رسم خط بر روی نقشه کلیک کنید',
        tools_length_measurement_edit: 'ویرایش',
        tools_length_measurement_edit_message: 'برای ویرایش خط بر روی آن کلیک کنید',
        tools_length_measurement_delete: 'حذف',
        tools_length_measurement_delete_message: 'برای حذف خط دوبار روی آن کلیک کنید'
      }
    }
  };

  export default messages;