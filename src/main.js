import Vue from 'vue';
import App from './App.vue';
import { BootstrapVue, BootstrapVueIcons  } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css'
import VueRouter from "vue-router";
import VueI18n from 'vue-i18n';
import messages from './assets/multilingualFile';
import router from './routes/routes';
import axios from 'axios';
import Multiselect from 'vue-multiselect';
import VuePersianDatetimePicker from 'vue-persian-datetime-picker';

import 'vue-multiselect/dist/vue-multiselect.min.css';
Vue.config.productionTip = false

//Axios: Setting to use axios globally
window.axios = axios;
window.axios.defaults.baseURL = 'https://api.example.com';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
Vue.prototype.$http = axios;



//Component: Register global component in vue
Vue.component('multiselect', Multiselect)
Vue.component('date-picker', VuePersianDatetimePicker);

//VueI18n: This library is used for supporting multilingual
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: 'fa', // set locale
  messages, // set locale messages
})

//BootstrapVue: This library is used to have bootstrap  as well as bootstrap icons
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

//VueRouter: This library is used to control routes
Vue.use(VueRouter)

//eventBus: Eventbus to dispatch events and all childs have access to data
export const eventBus = new Vue({
  methods: {
    resetZoom(zoom) {
      this.$emit('zoomWasReset', zoom)
    }
  }
})

//Vue: Define vue for the website
new Vue({
  el: '#app',
  i18n,
  router,
  render: h => h(App),
})

